const webpack = require('webpack');
const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
    entry: ['./src/app.js'],
    resolve: {
        extensions: ['*', '.js', '.jsx']
    },
    output: {
        filename: 'app.bundle.js',
        path: path.resolve(__dirname, 'dist')
    },
    module: {
        rules: [
            {
                test: /\.(js|jsx)$/,
                exclude: /node_modules/,
                use: ['babel-loader']
            },
            {
                test: /\.scss$/,
                use: [
                    "style-loader", //outputs our CSS into a <style> tag in the document.
                    "css-loader", //parses the CSS into JavaScript and resolves any dependencies.
                    "sass-loader" //transforms Sass into CSS.
                ]
            },
        ]
    },
    devServer: {
        port: 3000,
        watchContentBase: true
    },
    plugins: [new HtmlWebpackPlugin({ template: 'index.html' })],
};