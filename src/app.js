import React from 'react';
import ReactDOM from 'react-dom';
import App from './js/components/App';

import './scss/global.scss'

ReactDOM.render(
    <App />,
    document.getElementById('app')
);