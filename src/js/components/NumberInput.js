import React from 'react';

import '../../scss/input.scss';

const NumberInput = ({
    id,
    label,
    value,
    onChange,
}) => (
    <div className="input__field flex align--end">
        <input
            id={id}
            name={id}
            placeholder="00"
            type="number"
            value={value}
            onChange={onChange}
        />
        <label
            className="input__label"
            htmlFor={id}
        >
            {label}
        </label>
    </div>
);

export default NumberInput;