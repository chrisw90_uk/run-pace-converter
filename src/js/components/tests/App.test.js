import React from 'react';
import { render, screen, fireEvent, act } from "@testing-library/react";
import App from '../App';

describe("App", () => {
  it("renders", () => {
    render(<App />);
    expect(screen.getByText(/run pace converter/i)).toBeDefined();
  })
  it("updates mile pace when km pace is set", () => {
    render(<App />);
    const kmMins = screen.getAllByLabelText(/minutes/i)[0];
    const kmSecs = screen.getAllByLabelText(/seconds/i)[0];
    fireEvent.change(kmMins, { target: { value: "6" } });
    fireEvent.change(kmSecs, { target: { value: "22" } });
    const miMins = screen.getAllByLabelText(/minutes/i)[1];
    const miSecs = screen.getAllByLabelText(/seconds/i)[1];
    expect(miMins.value).toBe("10");
    expect(miSecs.value).toBe("15");
  })
  it("handles mile minutes being 0", () => {
    render(<App />);
    const kmSecs = screen.getAllByLabelText(/seconds/i)[0];
    fireEvent.change(kmSecs, { target: { value: "30" } });
    const miMins = screen.getAllByLabelText(/minutes/i)[1];
    expect(miMins.value).toBe("");
  })
  it("increments mile pace to next minute when conversion pace has 60 seconds", () => {
    render(<App />);
    const kmMins = screen.getAllByLabelText(/minutes/i)[0];
    const kmSecs = screen.getAllByLabelText(/seconds/i)[0];
    fireEvent.change(kmMins, { target: { value: "4" } });
    fireEvent.change(kmSecs, { target: { value: "21" } });
    const miMins = screen.getAllByLabelText(/minutes/i)[1];
    const miSecs = screen.getAllByLabelText(/seconds/i)[1];
    expect(miMins.value).toBe("7");
    expect(miSecs.value).toBe("");
  })
  it("updates km pace when mil pace is set", () => {
    render(<App />);
    const miMins = screen.getAllByLabelText(/minutes/i)[1];
    const miSecs = screen.getAllByLabelText(/seconds/i)[1];
    fireEvent.change(miMins, { target: { value: "10" } });
    fireEvent.change(miSecs, { target: { value: "15" } });
    const kmMins = screen.getAllByLabelText(/minutes/i)[0];
    const kmSecs = screen.getAllByLabelText(/seconds/i)[0];
    expect(kmMins.value).toBe("6");
    expect(kmSecs.value).toBe("22");
  })
  it("handles km minutes being 0", () => {
    render(<App />);
    const miSecs = screen.getAllByLabelText(/seconds/i)[1];
    fireEvent.change(miSecs, { target: { value: "10" } });
    const kmMins = screen.getAllByLabelText(/minutes/i)[0];
    expect(kmMins.value).toBe("");
  })
  it("increments km pace to next minute when conversion pace has 60 seconds", () => {
    render(<App />);
    const miMins = screen.getAllByLabelText(/minutes/i)[1];
    const miSecs = screen.getAllByLabelText(/seconds/i)[1];
    fireEvent.change(miMins, { target: { value: "1" } });
    fireEvent.change(miSecs, { target: { value: "36" } });
    const kmMins = screen.getAllByLabelText(/minutes/i)[0];
    const kmSecs = screen.getAllByLabelText(/seconds/i)[0];
    expect(kmMins.value).toBe("1");
    expect(kmSecs.value).toBe("");
  })
  it("prevents numbers over 59 from being inputted", () => {
    render(<App />);
    const input = screen.getAllByLabelText(/seconds/i)[1];
    fireEvent.change(input, { target: { value: "34" } });
    fireEvent.change(input, { target: { value: "61" } });
    expect(input.value).toBe("34");
  })
  it("resets converted values to empty", () => {
    render(<App />);
    const miMins = screen.getAllByLabelText(/minutes/i)[1];
    const miSecs = screen.getAllByLabelText(/seconds/i)[1];
    fireEvent.change(miMins, { target: { value: "10" } });
    fireEvent.change(miSecs, { target: { value: "15" } });
    const kmMins = screen.getAllByLabelText(/minutes/i)[0];
    const kmSecs = screen.getAllByLabelText(/seconds/i)[0];
    expect(kmMins.value).toBe("6");
    expect(kmSecs.value).toBe("22");
    fireEvent.change(miMins, { target: { value: "" } });
    fireEvent.change(miSecs, { target: { value: "" } });
    expect(kmMins.value).toBe("");
    expect(kmSecs.value).toBe("");
  })
  it("handles 0 values", () => {
    render(<App />);
    const miMins = screen.getAllByLabelText(/minutes/i)[1];
    const miSecs = screen.getAllByLabelText(/seconds/i)[1];
    fireEvent.change(miMins, { target: { value: "0" } });
    fireEvent.change(miSecs, { target: { value: "0" } });
    const kmMins = screen.getAllByLabelText(/minutes/i)[0];
    const kmSecs = screen.getAllByLabelText(/seconds/i)[0];
    expect(kmMins.value).toBe("");
    expect(kmSecs.value).toBe("");
    fireEvent.change(kmMins, { target: { value: "0" } });
    fireEvent.change(kmSecs, { target: { value: "0" } });
    expect(miMins.value).toBe("");
    expect(miSecs.value).toBe("");
  })
})