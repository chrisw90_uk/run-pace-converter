import React, { useState } from 'react';
import Converter from "./Converter";
import RaceTimes from "./RaceTimes";

const App = () => {
    const [kmPaceInSeconds, setKmPaceInSeconds] = useState(0);
    const [fields, setFields] = useState({
        kmMins: '',
        kmSecs: '',
        miMins: '',
        miSecs: '',
    });

    const updateConversion = (field, value, unit) => {
        const data = {
            ...fields,
            [field]: value,
        };
        if (unit === 'km') {
            const seconds = (data.kmMins * 60) + data.kmSecs;
            const mileSeconds = seconds * 1.609;
            let miMins = Math.floor(mileSeconds / 60) || '';
            let miSecs = Math.round(mileSeconds % 60) || '';
            if (miSecs === 60) {
                miSecs = '';
                miMins++;
            }
            setFields({ ...data, miMins, miSecs });
            setKmPaceInSeconds(seconds);
        } else {
            const seconds = (data.miMins * 60) + data.miSecs;
            const kmSeconds = seconds / 1.609;
            let kmMins = Math.floor(kmSeconds / 60) || '';
            let kmSecs = Math.round(kmSeconds % 60) || '';
            if (kmSecs === 60) {
                kmSecs = '';
                kmMins++;
            }
            setFields({ ...data, kmMins, kmSecs });
            setKmPaceInSeconds(kmSeconds);
        }
    };

    return (
        <main className="full flex justify--center align--center">
            <div>
                <h1 className="text--center">
                    Run Pace Converter
                </h1>
                <div className="flex flex--wrap justify--center align--center">
                    <section className="col">
                        <Converter
                            kmMins={fields.kmMins}
                            kmSecs={fields.kmSecs}
                            miMins={fields.miMins}
                            miSecs={fields.miSecs}
                            updateConversion={updateConversion}
                        />
                    </section>
                    <section className="col flex align--center flex--column flex--grow">
                        <RaceTimes kmPaceInSeconds={kmPaceInSeconds} />
                    </section>
                </div>
            </div>
        </main>
    )
};

export default App;
