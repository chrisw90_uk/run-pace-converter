import React from 'react';
import '../../scss/race.scss';

const raceTimeFromKmPace = (kmPaceInSeconds, distanceInKm) => {
    const raceTimeInSeconds = kmPaceInSeconds * distanceInKm;
    const raceTimeAsString = new Date(raceTimeInSeconds * 1000).toISOString().substr(11, 8);
    const raceTimeAsArr = raceTimeAsString.split(':');
    if (raceTimeAsArr[0] === '00') raceTimeAsArr.shift();
    return raceTimeAsArr.join(':');
};

const DISTANCES = {
    '5K': 5,
    '10K': 10,
    '10 Miles': 16.0934,
    'Half Marathon': 21.0975,
    'Marathon': 42.195,
};

const RaceTimes = ({
    kmPaceInSeconds,
}) => (
    <div className="races flex flex--column flex--grow">
        <h2 className="races__title">
            Approximate Race Times
        </h2>
        <ul className="races__list flex flex--column flex--grow justify--between">
            {Object.entries(DISTANCES).map(([title, distance]) => (
                <li
                    className="races__list-item flex justify--between"
                    key={title}
                >
                    <label>{title}</label>
                    <span>{raceTimeFromKmPace(kmPaceInSeconds, distance)}</span>
                </li>
            ))}
        </ul>
    </div>
);

export default RaceTimes;