import React from 'react';
import NumberInput from "./NumberInput";

const sanitise = (oldValue, newValue) => {
    if (!newValue) return '';
    const newValueAsNumber = Number.parseInt(newValue, 10);
    if (Number.isNaN(newValueAsNumber) || newValueAsNumber > 59) {
        return oldValue;
    }
    return newValueAsNumber;
};

const Converter = ({
    kmMins,
    kmSecs,
    miMins,
    miSecs,
    updateConversion
}) => (
    <React.Fragment>
        <div className="input flex align--end">
            <NumberInput
                id="kmMins"
                label="minutes"
                value={kmMins}
                onChange={e => updateConversion(
                    'kmMins',
                    sanitise(kmMins, e.target.value),
                    'km'
                )}
            />
            <NumberInput
                id="kmSecs"
                label="seconds"
                value={kmSecs}
                onChange={e => updateConversion(
                    'kmSecs',
                    sanitise(kmSecs, e.target.value),
                    'km'
                )}
            />
            <span className="input__unit">
                /km
            </span>
        </div>
        <h2 className="text--center">
            is approximately
        </h2>
        <div className="input flex align--end">
            <NumberInput
                id="miMins"
                value={miMins}
                onChange={e => updateConversion(
                    'miMins',
                    sanitise(miMins, e.target.value),
                    'mi'
                )}
                label="minutes"
            />
            <NumberInput
                id="miSecs"
                value={miSecs}
                onChange={e => updateConversion(
                    'miSecs',
                    sanitise(miSecs, e.target.value, 10),
                    'mi'
                )}
                label="seconds"
            />
            <span className="input__unit">
                /mi
            </span>
        </div>
    </React.Fragment>
);

export default Converter;