const config = {
  verbose: true,
  moduleFileExtensions: ['js', 'test.js'],
  modulePathIgnorePatterns: ["spec.js"],
  moduleNameMapper: {
    "\\.(css|scss)$": "<rootDir>/__mocks__/styleMock.js"
  },
  testEnvironment: 'jsdom',
  collectCoverage: true
};

module.exports = config;